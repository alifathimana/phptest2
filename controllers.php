<?php

/**
 * @file
 * This is controllers.php.
 */

require 'model.php';

/**
 * This is class controller.
 */
class Controller extends Model {

  /**
   * This is function to display login page.
   */
  public function loginpage() {
    require_once 'login.php';
  }

  /**
   * This is to verify the admin.
   */
  public function adminverify() {
    $link = $this->openDatabaseConnection();
    $this->verifyAdmin($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Function to add movie.
   */
  public function addmovie() {
    $link = $this->openDatabaseConnection();
    $movies = $this->movieadd($link);
    $this->closeDatabaseConnection($link);
    require_once 'templates/addmovies.tpl.php';
  }

  /**
   * Function to insert movie.
   */
  public function insertmovie() {
    $link = $this->openDatabaseConnection();
    $this->insertmov($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Function to logout.
   */
  public function logout() {
    session_destroy();
    $_SESSION['username'] = "";
    $_SESSION['password'] = "";
    session_unset();
    require 'login.php';
  }

  /**
   * Function to remove movie.
   */
  public function removemovie($c) {
    $link = $this->openDatabaseConnection();
    $this->movieremove($c, $link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Function to add theatre screen.
   */
  public function addtheatrescreen() {
    $link = $this->openDatabaseConnection();
    $this->addscreen($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Function to list screen.
   */
  public function screenlist($c) {
    $link = $this->openDatabaseConnection();
    $this->listscreen($c, $link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Function to list screen.
   */
  public function insertscreen() {
    $link = $this->openDatabaseConnection();
    $this->screeninsert($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * User start page.
   */
  public function userhomepage() {
    $link = $this->openDatabaseConnection();
    $this->userhome($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Movie details page.
   */
  public function moviedetails($id) {
    $link = $this->openDatabaseConnection();
    $this->detailsmovie($id, $link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Show dropdown theatre list.
   */
  public function movieshow() {
    $link = $this->openDatabaseConnection();
    $this->showmovie($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Show dropdown screen list.
   */
  public function screenshow() {
    $link = $this->openDatabaseConnection();
    $this->showscreen($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Show dropdown timing list.
   */
  public function timeshow() {
    $link = $this->openDatabaseConnection();
    $this->showtime($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Show confirmation page.
   */
  public function confirmbooking() {
    $link = $this->openDatabaseConnection();
    $this->bookingconfirm($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * Show confirmation page.
   */
  public function bookticket() {
    $link = $this->openDatabaseConnection();
    $this->ticketbooking($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * User details page.
   */
  public function userdetails($id) {
    $link = $this->openDatabaseConnection();
    $this->details($id, $link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * User listing page.
   */
  public function userlist() {
    $link = $this->openDatabaseConnection();
    $this->listuser($link);
    $this->closeDatabaseConnection($link);
  }

  /**
   * User listing page.
   */
  public function addtheatre() {
    $link = $this->openDatabaseConnection();
    $this->theatreadd($link);
    $this->closeDatabaseConnection($link);
  }

}
