<?php

/**
 * This is adminhomepage.tpl.php file.
 */
?> 
<html>
<body>
  <h1>Movie Ticket Booking System</h1>
  <br><br>
  <a href = '/index.php/userlist'>View user list</a><br><br>
  <a href = '/index.php/addmovie'>Add a new movie</a><br><br>
  <a href = '/index.php/addtheatrescreen'>Add a new theatre or screen</a><br><br><br>
  <a href = '/index.php/logout'>Logout</a>
</body>
</html>
<?php 
require 'templates/layout.tpl.php';
require 'templates/admin.tpl.php';
?>