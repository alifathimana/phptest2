<?php
/**
 * @file
 * This is userdetails.tpl.php
 */
?>
<html>
<table>
<?php
$i = 1;
foreach ($userdetails as $user) {
  echo "<tr><td>{$i}</td></tr>";
  echo "<tr><td>Movie Name: </td><td>{$user['name']}</td></tr>";
  echo "<tr><td>Theatre: </td><td>{$user['theatrename']}</td></tr>";
  echo "<tr><td>Screen: </td><td>{$user['screenname']}</td></tr>";
  echo "<tr><td>Time: </td><td>{$user['time']}</td></tr>";
  echo "<tr><td>Seats: </td><td>{$user['seats']}</td></tr>";
  echo "<tr><td>Amount: </td><td>{$user['amount']}</td></tr>";
  echo "<br><br><br>";
}
?>
</table>
<br><br> 
<a href = '/index.php/logout'>Logout</a>
</html>
<?php
require 'templates/layout.tpl.php';
?>