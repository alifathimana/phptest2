<?php

/**
 * @file
 * This is index.php.
 */

require 'controllers.php';
$controller = new Controller();

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

if ($uri == '/') {
  $controller->loginpage();
}
elseif ($uri == '/index.php/adminverify') {
  $controller->adminverify();
}
elseif (($uri == '/index.php/userdetails') && isset($_GET['link'])) {
  $id = $_GET['link'];
  $controller->userdetails($id);
}
elseif ($uri == '/index.php/addmovie') {
  $controller->addmovie();
}
elseif ($uri == '/index.php/userlist') {
  $controller->userlist();
}
elseif ($uri == '/index.php/insertmovie') {
  $controller->insertmovie();
}
elseif ($uri == '/index.php/addtheatrescreen') {
  $controller->addtheatrescreen();
}
elseif ($uri == '/index.php/logout') {
  $controller->logout();
}
elseif (($uri == '/index.php/theatre') && isset($_GET['link'])) {
  $c = $_GET['link'];
  $controller->screenlist($c);
}
elseif (($uri == '/index.php/removemovie') && isset($_GET['link'])) {
  $c = $_GET['link'];
  $controller->removemovie($c);
}
elseif ($uri == '/index.php/insertscreen') {
  $controller->insertscreen();
}
elseif ($uri == '/index.php/user') {
  $controller->userhomepage();
}
elseif (($uri == '/index.php/user/moviedetails') && isset($_GET['link'])) {
  $id = $_GET['link'];
  $controller->moviedetails($id);
}
elseif (($uri == '/index.php/user/booktickets') && isset($_GET['link'])) {
  $id = $_GET['link'];
  $controller->bookticket($id);
}
elseif (($uri == '/index.php/user/movieshow')) {
  $controller->movieshow();
}
elseif ($uri == '/index.php/user/confirmbooking') {
  $controller->confirmbooking();
}
elseif ($uri == '/index.php/user/bookticket') {
  $controller->bookticket();
}
elseif ($uri == '/index.php/addtheatre') {
  $controller->addtheatre();
}
else {
  echo $uri;
  header('http/1.1 404 Not Found');
  echo "<h1>page not found</h1>";
}