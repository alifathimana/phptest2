<?php

/**
 * @file
 * This is model.php file.
 */

 /**
  * This is class model.
  */
class Model {

  /**
   * This is to open database connection.
   */
  public function openDatabaseConnection() {
    $servername = "localhost";
    $dbname = "phptest2";
    $username = "root";
    $password = "root";
    $link = new PDO("mysql:servername=$servername;dbname=$dbname", $username, $password);
    $link->setAttribute(PDO:: ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $link;
  }

  /**
   * This is to close database connection.
   */
  public function closeDatabaseConnection() {
    $link = NULL;
  }

  /**
   * This is function to open login page.
   */

  public function loginpage() {
    require 'login.php';
  }

  /**
   * This is to verify the admin.
   */
  public function verifyAdmin($link) {

    $username = htmlentities($_POST['username']);
    $password = htmlentities($_POST['password']);
    $stmt = $link->prepare("select * from admin_login where username = :username and password = :password");
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', $password);
    $stmt->execute();
    $admin = $stmt->fetchAll();
    if ($admin) {
      $_SESSION['username'] = $username;
      $_SESSION['password'] = $password;
      require 'templates/adminhomepage.tpl.php';
    }
    else {
      header("location: http://phptest2/");
    }
  }

  /**
   * Fnction to add movie.
   */
  public function movieadd($link) {
    $stmt = $link->prepare("select * from movies where flag = 0");
    $stmt->execute();
    $movie = $stmt->fetchAll();
    return $movie;
  }

  /**
   * Function to insert movie.
   */
  public function insertmov($link) {
    $name = htmlentities($_POST['moviename']);
    $type = htmlentities($_POST['movietype']);
    $stmt = $link->prepare("insert into movies (Type, name) values (:type, :name)");
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':name', $name);
    $stmt->execute();
    header("Location: http://phptest2/index.php/addmovie");
  }

  /**
   * Remove movie.
   */
  public function movieremove($c, $link) {
    $fl = 1;
    $stmt = $link->prepare("update movies set flag = :flag where id = :id");
    $stmt->bindParam(':id', $c);
    $stmt->bindParam(':flag', $fl);
    $stmt->execute();
    header("Location: http://phptest2/index.php/addmovie");
  }

  /**
   * Add screen.
   */
  public function addscreen($link) {
    $stmt = $link->prepare("select * from theatre");
    $stmt->execute();
    $theatre = $stmt->fetchAll();
    require 'templates/theatre.tpl.php';
  }

  /**
   * List screen.
   */
  public function listscreen($c, $link) {
    $stmt = $link->prepare("select * from screens where theatreid = :id");
    $stmt->bindParam(':id', $c);
    $stmt->execute();
    $screens = $stmt->fetchAll();
    require 'templates/screens.tpl.php';
  }

  /**
   * Function to insert screen.
   */
  public function screeninsert($link) {
    $name = htmlentities($_POST['name']);
    $seats = htmlentities($_POST['seats']);
    $id = htmlentities($_POST['id']);
    $stmt = $link->prepare("insert into screens (screenid, theatreid, screenname, total_seats) values (:scid, :thid, :name, :tot)");
    $stmt->bindParam(':scid', $id);
    $stmt->bindParam(':thid', $id);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':tot', $seats);
    $stmt->execute();
    header("location: http://phptest2/index.php/theatre?link=$id");

  }
  /**
   * User home page.
   */
  public function userhome($link) {
    $stmt = $link->prepare("select * from movies where flag = 0");
    $stmt->execute();
    $movie = $stmt->fetchAll();
    require 'templates/userhome.tpl.php';
  }

  /**
   * Movie details.
   */
  public function detailsmovie($id, $link) {
    $stmt = $link->prepare("select a.name, a.Type,
                          b.seats, b.time, b.amount, b.id, c.screenname, 
                          d.theatrename, d.location
                          from movies a
                          join show_times b
                          on a.id = b.movieid
                          join screens c on
                          b.screenid = c.id
                          join theatre d on 
                          c.theatreid = d.id
                          where a.id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $details = $stmt->fetchAll();
    require 'templates/moviedetails.tpl.php';
  }

  /**
   * Ticket booking.
   */
  public function ticketbook($id, $link) {
    $stmt = $link->prepare("select seats from show_times where id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $sea = $stmt->fetchAll();
    require 'templates/booking.tpl.php';
  }

  /**
   * Ticket booking.
   */
  public function showmovie($link) {
    $id = htmlentities($_POST['movielist']);
    $stmt = $link->prepare("select a.id, a.time, b.screenname, c.theatrename from show_times a join screens b on a.screenid = b.id join theatre c on b.theatreid = c.id where a.movieid = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $a = $stmt->fetchAll();
    require 'templates/showtheatrelist.tpl.php';
  }

  /**
   * Ticket booking.
   */
  public function showscreen($link) {
    $movieid = $_POST['movieid'];
    $id = $_POST['theatrelist'];
    $stmt = $link->prepare("select distinct b.id, b.screenname from show_times a join screens b on a.screenid = b.id join theatre c on b.theatreid = c.id where b.theatreid = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $b = $stmt->fetchAll();
    require 'templates/showscreen.tpl.php';
  }

  /**
   * Ticket booking.
   */
  public function showtime($link) {
    $movieid = htmlentities($_POST['movieid']);
    $theatreid = htmlentities($_POST['theatreid']);
    $id = htmlentities($_POST['screenlist']);
    $stmt = $link->prepare("select a.id, a.time from show_times a join screens b on a.screenid = b.id join theatre c on b.theatreid = c.id where b.id = :id and movieid = :id1");
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':id1', $movieid);
    $stmt->execute();
    $b = $stmt->fetchAll();
    require 'templates/showtiming.tpl.php';
  }

  /**
   * Booking confirm.
   */
  public function bookingconfirm($link) {
    $id = $_POST['theatrelist'];
    $stmt = $link->prepare("select a.id, a.amount, d.name, c.theatrename, b.screenname, c.location, a.time, a.seats from show_times a join screens b on a.screenid = b.id join theatre c on b.theatreid = c.id join movies d on a.movieid = d.id where a.id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $con = $stmt->fetchAll();
    require 'templates/bookingconfirmation.tpl.php';
  }

  /**
   * Booking confirm.
   */
  public function ticketbooking($link) {
    $seats = htmlentities($_POST['seats']);
    $name = htmlentities($_POST['name']);
    $mail = htmlentities($_POST['mail']);
    $id = htmlentities($_POST['id']);
    $stmt = $link->prepare("select a.screenid, a.id, a.seats, a.amount from show_times a where a.id = :id ");
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    $se = $stmt->fetchAll();
    $number = $se[0]['seats'];
    $am = $se[0]['amount'];
    if ($seats < $number) {
      $now = $number - $seats;
      $a = $seats * $am;
      $stmt = $link->prepare("insert into user_details (user_id, email) values (:user_id, :email)");
      $stmt->bindParam(':user_id', $id);
      $stmt->bindParam(':email', $mail);
      $stmt->execute();
      $stmt = $link->prepare("select max(id) as id from user_details");
      $stmt->execute();
      $userid = $stmt->fetchAll();
      $user = $userid[0]['id'];
      $screenid = $se[0]['screenid'];
      $showid = $se[0]['id'];
      $stmt = $link->prepare('insert into booking (userid, screenid, showid, seats, amount) values (:userid, :screenid, :showid, :seats, :amount)');
      $stmt->bindParam(':userid', $user);
      $stmt->bindParam(':screenid', $screenid);
      $stmt->bindParam(':showid', $showid);
      $stmt->bindParam(':seats', $seats);
      $stmt->bindParam(':amount', $a);
      $stmt->execute();
      $new = $number - $seats;
      $stmt = $link->prepare("update show_times set seats = :seats where id = :id");
      $stmt->bindParam(':seats', $new);
      $stmt->bindParam(':id', $id);
      $stmt->execute();
      $stmt = $link->prepare("select max(id) as bookingid from booking");
      $stmt->execute();
      $bkng = $stmt->fetchAll();
      $bookingid = $bkng[0]['bookingid'];
    }
    require 'templates/ticket.tpl.php';
  }

  /**
   * Details of user.
   */
  public function details($id, $link) {
    $stmt = $link->prepare('SELECT e.name, f.email, a.id, a.amount, a.seats, d.theatrename, d.location, c.screenname, b.time FROM `booking` a join show_times b on a.showid = b.id join screens c on b.screenid = c.id join theatre d on c.theatreid = d.id join movies e on b.movieid = e.id join user_details f on f.id = a.id where f.id = :id');
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $userdetails = $stmt->fetchAll();
    require 'templates/userdetails.tpl.php';
  }

  /**
   * List user.
   */
  public function listuser($link) {
    $stmt = $link->prepare("select * from user_details");
    $stmt->execute();
    $user = $stmt->fetchAll();
    require 'templates/userlisting.tpl.php';
  }

  /**
   * function to add theatre
   */
  public function theatreadd($link) {
    $id = rand(100,999);
    $name = $_POST['theatre'];
    $loc = $_POST['location'];
    $stmt = $link->prepare("insert into theatre (theatreid, theatrename, location) values (:thid, :name, :loc)");
    $stmt->bindParam('thid', $id);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':loc', $loc);
    $stmt->execute();
    header("Location: http://phptest2/index.php/addtheatrescreen");
   }

}
